<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Reportes\ConsultoresController@index')->name('/');

Route::group(['prefix'=>'comercial', 'namespace' => 'Reportes'],  function(){
	Route::get('performance-comercial','ConsultoresController@index')->name('comercial.performance-comercial');


});
