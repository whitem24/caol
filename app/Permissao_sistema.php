<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permissao_sistema extends Model
{
	public $timestamps = false;
    protected $table = 'permissao_sistema';
}
