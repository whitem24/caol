<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cao_os extends Model
{
    
    protected $table = 'cao_os';
    protected $primaryKey = 'co_os';


    public function faturas(){
    	return $this->hasMany('App\Cao_fatura','co_os');

}
}
