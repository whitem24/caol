<?php

namespace App\Http\Controllers\Reportes;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Carbon\Carbon;
use Validator;
use Carbon\CarbonPeriod;
Use App\Cao_fatura;
Use App\Cao_salario;
use App\Cao_usuario;
use App\Permissao_sistema;
use App\Cao_os;

class ConsultoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $consultores =  Cao_usuario::consultores()->get();
        $origin=$consultores;
        $target=null;
        $from=  Null;
        $to =  Null;
        $format=false;
        $ganancia_total_ordenes = [];
        $costo_fijo_promedio = 0;
        $costo_fijo_total = 0;
        $period = null;
        $comision_ordenes=null;
        $costo_fijo = null;


        if (count($request->all()) < 1) {
            $checked_consultor = "checked";
            $checked_cliente = "";
        }else{
            if ($request->per == "cliente") {
               $checked_cliente = "checked";
               $checked_consultor = "";
            }else{
               $checked_consultor = "checked";
               $checked_cliente = "";
            }
                $format = $request->format;
                $origin = isset($request->origin) ?  Cao_usuario::wherein('co_usuario', $request->origin)->get() : $consultores;
               
                $target_r = isset($request->target) ? $request->target : Null;
                if ($target_r !== null ) {
                    $target = Cao_usuario::wherein('co_usuario', $target_r)->get();
                }else{
                    $target = $target_r;
                }
                $from= isset($request->from) ? $request->from : Null;
                $to = isset($request->to) ? $request->to : Null;
                $date_f = Carbon::parse($from)->format("Y-m-d");
                $date_f_month = Carbon::parse($from)->format("Y-m");
                $date_t = Carbon::parse($to)->format("Y-m-d");
                $date_t_month = Carbon::parse($to)->format("Y-m");
                $period = CarbonPeriod::create($date_f_month,'1 month',$date_t_month);
                
                if (isset($target)) {
                    foreach ($target as $t => $consultores_t) {
                        $target[$t]->total_ganancias_netas = 0;
                        foreach ($period as $p=> $date) {
                            $periodo = $date->format('Y-m-d');

                            $new_carbon = Carbon::create($periodo);
                            $period_ends = $new_carbon->addMonth()->subDay()->format('Y-m-d');
                            $ordenes[$t][$p] = Cao_os::with(['faturas' => function($query) use ($periodo,$period_ends){
                                $query->select(DB::raw('co_os,valor,SUM(valor  - (valor*total_imp_inc/100)) as ganancias_orden,SUM((valor - (valor*total_imp_inc/100))*(comissao_cn/100) ) as comissao'))
                                ->groupBy('co_os')
                                ->groupBy('valor')
                                 ->where('data_emissao','>=',$periodo)
                                ->where('data_emissao','<=',$period_ends)
                                ->get();
                            }])
                            ->where('co_usuario',$target[$t]->co_usuario)
                            /*->where(function($q) use ($periodo, $period_ends) {
                                $q->whereBetween('dt_inicio', [$periodo, $period_ends]);
                            }) ,'SUM( (valor - (valor*total_imp_inc))  *  comissao_cn ) as comissao'*/
                            //->whereBetween('dt_inicio', [$periodo, $period_ends])
                            ->get();
                            $ganancia_orden = [];
                            $comision = [];

                            foreach ($ordenes[$t][$p] as $o  =>  $orden) {
                                foreach ($orden->faturas as $fatura) {
                                    if ($fatura->ganancias_orden > 0) {
                                        //return $ganancia_neta[$t];
                                            $ganancia_orden[$o] = $fatura->ganancias_orden;
                                            $comision[$o] = $fatura->comissao;
                                            $target[$t]->total_ganancias_netas += $fatura->ganancias_orden;
                                            $target[$t]->comision_total += $fatura->comissao;
                                            // echo $orden->co_os.'--- ';
                                            // echo $target[$t]->co_usuario.' '.$comision[$o] ;
                                            // echo '---';
                                            // echo $target[$t]->co_usuario.' '.$target[$t]->total_ganancias_netas.'<br>';
                                            break;

                                        
                                    }

                                }
                                    
                            }
                           
                            $ganancia_total_ordenes[$t][$p] = array_sum($ganancia_orden);
                            $ganancia_total_consultor[$t] = array_sum($ganancia_orden);
                            $comision_ordenes[$t][$p] = array_sum($comision);
                            
                        //
                        }
                        $costo_fijo[$t] = Cao_salario::where('co_usuario', $consultores_t->co_usuario)->first();
                        $target[$t]->total_costo_fijo = Cao_salario::where('co_usuario', $consultores_t->co_usuario)->sum('brut_salario') * count($period);
                        if (isset($costo_fijo[$t])) {
                            $costo_fijo_total += $costo_fijo[$t]->brut_salario *  count($period);
                        }
                        
                    }

                $costo_fijo_promedio = $costo_fijo_total /  count($target);
                }
                //dd(count($target)); 

                
                   

                
            

        }
          return response(view('reportes.consultores', ['consultores'=>$consultores,'checked_cliente'=>$checked_cliente,'checked_consultor'=>$checked_consultor ,'origin'=>$origin ,'target'=>$target, 'ganancia'=>$ganancia_total_ordenes, 'comision' => $comision_ordenes,'period'=>$period,'from'=>$from,'to'=>$to,'format'=>$format, 'costo_fijo' => $costo_fijo, 'costo_fijo_promedio' => $costo_fijo_promedio  ]));


    
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
