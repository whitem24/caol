<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cao_usuario extends Model
{
	public $timestamps = false;
    protected $table = 'cao_usuario';
    protected $dates = [
        'dt_nascimento', 'dt_admissao_empresa', 'dt_desligamento','dt_expiracao'
    ];

    public function scopeConsultores($query){

    	$query->select('cao_usuario.*')->join('permissao_sistema', function($join) {
            $join->on('permissao_sistema.co_usuario', '=', 'cao_usuario.co_usuario')
                 ->where('permissao_sistema.co_sistema','=', 1)
                 ->where('permissao_sistema.in_ativo','=', 'S')
                 ->whereBetween('permissao_sistema.co_tipo_usuario', [0,2]);

        });
    }
}
