<script type="text/javascript">
		$(document).ready(function($) {

    		$('#multiselect').multiselect();
    		$('#datepicker2').datepicker({
    			autoclose:true,
    			format:'M d yyyy'	
    		}); 
            $('#datepicker2').datepicker('setDate','01-01-2007')  
    		$('#datepicker1').datepicker({
    			autoclose:true,
    			format: 'M d yyyy',
                
    		});
            $("#datepicker1").datepicker('setDate','12-31-2007');
    	    $('.consultores-table').DataTable({

                "sorting" : false,
                "paging":   false,
                "searching":false,
                "info":     false
    
            });

            var format =  {!!json_encode($format)!!};
            var target = {!!json_encode($target)!!};

            if (format=="grafico") {
                //ChartJS
            var ctx = document.getElementById('barChart');
            var costo_fijo_promedio= {!!json_encode($costo_fijo_promedio)!!};
            console.log(target);
            var barChart = new Chart(ctx, {
            type: 'bar',
            data: {
               
                
                    labels:[]
                ,
                datasets: [{
                    label: 'Ganancia',
                    data: [],
                    backgroundColor: [
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff'
                    ],
                    borderColor: [
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff',
                        '#007bff'
                    ],
                    borderWidth: 1
                },{
                    label: 'Costo Promedio',
                    data: [],
                    backgroundColor: [
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545'
                    ],
                    borderColor: [
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545',
                        '#dc3545'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                            return 'R$ ' + value;},
                            beginAtZero: true
                        }
                    }]
                }
            }
            });
            function dataChart(){
                var consultores = [];
                var promedio = [];
                var ganancias = [];
                for (var i = 0; i < target.length; i++) {
                  consultores[i]= target[i].no_usuario;
                  ganancias[i]= Math.round(target[i].total_ganancias_netas,2);
                  promedio[i]= Math.round(costo_fijo_promedio,2);
                }
                barChart.data.labels = consultores ;
                barChart.data.datasets[0].data = ganancias;
                barChart.data.datasets[1].data = promedio;
                var max_ga = ganancias.reduce(function(a, b) { 
                    return Math.max(a, b);
                });
                var max_pro = promedio.reduce(function(a, b) { 
                    return Math.max(a, b);
                });
                var max =  Math.max(max_ga, max_pro);
                console.log(max);
                barChart.config.options.scales.yAxes[0].ticks.max = max ;
                barChart.update();  
            }
            dataChart();  
            }else if (format=="pizza")
            {
                var ctx = document.getElementById('pieChart');
                var  pieChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: [],
                        datasets: [{
                            data: [],
                            backgroundColor: []
                        }]    
                    }
                });

                function dataChartPie(){
                    var consultores = [];
                    var ganancias = [];
                    for (var i = 0; i < target.length; i++) {
                      consultores[i]= target[i].no_usuario;
                      ganancias[i]= Math.round(target[i].total_ganancias_netas,2);
                      pieChart.data.labels.push(consultores[i]);
                      pieChart.data.datasets[0].backgroundColor.push(randomColor());
                    }
                    pieChart.data.datasets[0].data = ganancias;
                    
                    console.log(ganancias);


                    pieChart.update();  
                }
                function randomColor() {
                    var letters = '0123456789ABCDEF'.split('');
                    var color = '#';
                    for (var i = 0; i < 6; i++ ) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }
                dataChartPie();  
            }
                  

});
       
</script>
