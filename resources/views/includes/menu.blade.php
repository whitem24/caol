  <nav class="navbar navbar-expand-lg navbar-dark bg-blue static-top">
    <div class="container">
      <a class="navbar-brand" href="{{ route('/') }}"><img src="/img/logo.png" width="100" height="auto" alt="Agence" class="  img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link" href="#">Agence
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Projetos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Administrativo</a>
          </li>
          <li class="  show nav-item dropdown">
        <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">Comercial</a>
        <div class="dropdown-menu">
            <a class="dropdown-item active" href="{{ route('comercial.performance-comercial') }}">Performance Comercial</a>
          <div class="dropdown-divider"></div>
        </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Financeiro</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="#">Usuario</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="material-icons">exit_to_app</i></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>