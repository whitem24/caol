<div class="box-blue  box col-12">    
              		<div class="box-header with-border text-center">
              			<h3>Resultados:</h3>
              	    </div>
              		<div class="box-body ">
              	    @if(isset($target))
                    @foreach($target as $t => $consultor)
                    @php
                    	$target[$t]->total_costo_fijo = 0;
                    @endphp              		
					<table  class="display consultores-table" style="width:100%">
	        			<thead>
	        				<tr>
	        					<th colspan="7" class="bg-primary text-center text-white">{{$consultor->no_usuario}}</th>
	        				</tr>
	            			<tr>
	                			
	                			<th>Periodo</th>
	                			<th>Receita liquida</th>
	                			<th>Custo fixo</th>
	                			<th>Comissao</th>
	                			<th>Lucro</th>
	            			</tr>
	        			</thead>
	        			<tbody>
	        				@foreach($period as $p => $periodo)  
	            			<tr>
	                			
	                			<td>{{$periodo->format('M Y')}}</td>
	                			<td>R$ {{isset($ganancia[$t][$p]) ? round($ganancia[$t][$p],2) : 0}}</td>
	                			<td>R$ {{isset($costo_fijo[$t]->brut_salario) ? $costo_fijo[$t]->brut_salario : 0}}</td>
	                			<td>R$ {{isset($comision[$t][$p]) ? round($comision[$t][$p],2) : 0}}</td>
	                			<td>R$ {{round($ganancia[$t][$p] - $comision[$t][$p] - (isset($costo_fijo[$t]->brut_salario) ? $costo_fijo[$t]->brut_salario:0),2)}} </td>


	                			@php 
	                				if ($costo_fijo[$t] !== null) {

	                						$target[$t]->total_costo_fijo += $costo_fijo[$t]->brut_salario;
	                				}
	                			@endphp
	            			</tr>

	            			@endforeach
	            			<tfoot>
	            			<tr>
	            				<th>
	            					Saldo
	            				</th>
	            				<th>R$ {{round($target[$t]->total_ganancias_netas, 2)}}</th>
	            				<th>R$ {{	$target[$t]->total_costo_fijo}}</th>
	            				<th>R$ {{round($target[$t]->comision_total,2)}}</th>
	            				<td>R$ {{ round(($target[$t]->total_ganancias_netas - $target[$t]->comision_total - 	$target[$t]->total_costo_fijo ),2 )}} </td>
	            			</tr>
	            			</tfoot>
	            		</tbody>
	            	</table> 
	            	<br>
	            	@endforeach
	            	@endif
	            	</div>
	        	</div>