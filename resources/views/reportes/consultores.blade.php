@extends('main')

@section('content')
	<div class="row mt-5">
		<form action="{{route('comercial.performance-comercial')}}" id="frm">
		   <div class="box-blue text-center box col-12">    
              <div class="box-header with-border">
              	<h3>Desempeño de consultores</h3>
              </div>
              <div class="box-body mt-3">
  					<div class="form-check form-check-inline border border-primary text-primary">
					  <label class="form-check-label mr-2" for="inlineRadio1">
					  	<input onChange="document.getElementById('frm').submit()" class="form-check-input  ml-2" type="radio" name="per" id="inlineRadio1 datepicker" value="consultor" {{$checked_consultor}}>Por consultor
					  </label>
					</div>
					<div class="form-check form-check-inline  text-primary  border border-primary ">
					  <label class="form-check-label mr-2" for="inlineRadio2">
					  	<input  class="form-check-input  ml-2"  {{$checked_cliente}}  type="radio" name="per" id="inlineRadio2" value="cliente" onChange="document.getElementById('frm').submit()">Por cliente
					  </label>
					</div>
					<div class="card-deck d-flex justify-content-center mt-5">
						<div class=" card-group mt-1 row">
							<div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3  text-white card bg-primary mb-3" >
							  <div class="card-header border-white text-center">Periodo</div>
							  <div class="card-body pt-4">
							  	<div class=" mt-3">
				  					<label class="sr-only" for="inlineFormInputGroupUsername2"></label>
				  					<div class="input-group mb-2 mr-sm-2">
				    					<div class="input-group-prepend">
				      						<div class="input-group-text">Desde</div>
				    					</div>
				    				<input name="from" type="text" value="{{$from}}" class="form-control" id="datepicker2" placeholder="Seleccione">
				  					</div>

				  					<label class="sr-only" for="inlineFormInputGroupUsername2"></label>
				  					<div class="input-group mb-2 mr-sm-2">
				    					<div class="input-group-prepend">
				      						<div class="input-group-text">Hasta	</div>
				    					</div>
				    				<input type="text" value="{{$to}}" name="to"  class="form-control" id="datepicker1" placeholder="Seleccione">
				  					</div>

								</div>
							  </div>
							</div>
							<div class=" col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 card text-white  bg-primary mb-3" >
							  <div class=" text-center border-white card-header">Consultores</div>
							  <div class="card-body pt-4">
								<div class="row text-center">
								    <div class="col-md-5 col-12 col-lg-5 col-xl-5 col-sm-12  ml-n3">
								        <select name="origin[]" id="multiselect" class="  w-select form-control" size="6" multiple="multiple">
								         @if(isset($origin))	
								        	@foreach($consultores as $consultor)
								        		@foreach($origin as $or)
								        			@if($or->co_usuario ==$consultor->co_usuario)
														<option value="{{$consultor->co_usuario}} ">{{$consultor->no_usuario}}
														</option>
													@endif
                   							    @endforeach
                 							@endforeach
                 						@endif
								        </select>
								    </div>
								    <div class=" mt-n4 col-md-1 ml-4">
								         			<button type="button" id="multiselect_rightAll" class="btn btn-block mt-4 pt-0 py-0"><i class="material-icons"><span class="mr-n3 text-white">chevron_right</span><span class="text-white">chevron_right</span> </i></button>
											        <button type="button" id="multiselect_rightSelected" class="my-0 py-0 btn btn-block"><i class="text-white material-icons">chevron_right</i></button>
											        <button type="button" id="multiselect_leftSelected" class="my-0	 py-0 btn btn-block"><i class=" text-white material-icons">chevron_left</i></button>
											        <button type="button" id="multiselect_leftAll" class="btn btn-block my-0 py-0"><i class="text-white  material-icons"><span class="mr-n3">chevron_left</span><span>chevron_left</span></i></button>
								    </div>
								    <div class="ml43 col-md-5 ">
								        <select  name="target[]" id="multiselect_to" class="form-control w-select" size="6" multiple="multiple">
                                        @if(isset($target))
                                        	@foreach($consultores as $consultor)
								        		@foreach($target as $ta)
								        			@if($ta->co_usuario ==$consultor->co_usuario)
														<option value="{{$consultor->co_usuario}} ">{{$consultor->no_usuario}}
														</option>
													@endif
                   							    @endforeach
                 							@endforeach
                 						@endif
					                    </select>
					  				

								        	
                 					</div>
								</div>


							  </div>
							</div>
 

							<div class="card bg-transparent border border-primary mb-3 col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
							  <div class=" text-primary card-header border-primary">Formato</div>
							  <div class="card-body pt-4 mt-1 pb-0  border-primary">
								
								<div class="d-flex p-3 bg-light text-white pl-4 justify-content-center ml-n2">
				  					<div class="p-2 bg-transparent border-primary border px-3">
				  						<button type="submit" value="relatorio" name="format" class="  btn btn-sm btn-blue mb-2 "><i class="material-icons">assignment</i><br>Relatorio</button>
				  					</div>
				  					<div class="p-2 bg-transparent border-primary border px-4">
				  						<button type="submit" value="grafico" name="format" class="mt-0 btn btn-sm  btn-blue mb-2 "><i class="material-icons">assessment</i><br>Grafico</button>
				  					</div>
				  					<div class="p-2 bg-transparent border-primary border px-4">
				  						<button  name="format" type="submit" class="mt-0 btn btn-sm btn-blue mb-2 " value="pizza"><i class="material-icons">donut_small</i><br>Pizza</button>
				  					</div>
								</div>	
									

							  </div>
							</div>
 

					    </div>
					</div>
				</div>
           	</div>
           	@if(isset($format))
           	   	@if($format=='relatorio')
					@include('includes.relatorio')
				@elseif($format=='grafico')
					@include('includes.grafico')
				@elseif($format=='pizza')
           	   		@include('includes.pizza')
           	   	@endif
		   	
	        @endif
	        </form>
			
       	   </div>

	</div>
@stop
@push('script')
	<script src="{{asset('bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
	<script src="/js/multiselect.js" type="text/javascript"></script>
	<script type="text/javascript" src="/js/DataTables/js/dataTables.boostrap4.min.js"></script>
	<script type="text/javascript" src="/js/dist/Chart.min.js"></script>
	@include('includes.reporte-consultores-script')
@endpush
@push('css')
	
	<link rel="stylesheet" href="{{asset('bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
	<link rel="stylesheet"  href="/js/DataTables/css/jquery.dataTables.min.css" ></link>
	<link rel="stylesheet"  href="/js/dist/Chart.min.css" ></link>

@endpush

