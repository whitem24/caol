
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{config('app.name')}}</title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/jquery-ui.css">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/datatables.min.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  @stack('css')
  <link rel="shortcut icon"  type="image/x-icon" href="{{asset('img/favicon.ico')}}">
</head>

<body>

  <!-- Navigation -->
  @include('includes.menu')
  <!-- Navigation -->

  <!-- Content -->
  <div class="mx-5">
  	@yield('content')
  </div>
  <!-- Content --> 
  

  <!-- Bootstrap core JavaScript -->
  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>
  <script src="/js/datatables.min.js"></script>
  <script src="/js/style.js"></script>
  @stack('script')
  
</body>

</html>
