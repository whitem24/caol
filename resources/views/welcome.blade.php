@extends('main')

@section('content')

            <div class="text-center mt20">
              <img src="/img/logo.gif" width="150" height="auto" alt="Agence" class="  img-fluid"></div>
            </div>
  
         <!-- Horizontal Form -->
          <div class="row justify-content-center  mt-5">
            <div class="box-blue  box col-4">    
            <div class="box-header with-border"></div>
  
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body ml-5">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <button type="submit" class="btn btn-default">Cancel</button>
                <button type="submit" class="btn btn-blue pull-right">Sign in</button>
              </div>
              <!-- /.box-footer -->
            </form>
            </div>
            </div>
        
          <!-- /.box -->
        
  

@stop





